### This is the implementation of test task for Clojure Developer position @ Attendify by Volodymyr Gamula


## Information about the task
[https://gist.github.com/kachayev/f0b110340fb18ccef469](https://gist.github.com/kachayev/f0b110340fb18ccef469)


## Tasks
* [Dribbble Stats](https://bitbucket.org/vgamula/attendify-test-task/src/master/dribbble-stats/?at=master)
* [URL Match](https://bitbucket.org/vgamula/attendify-test-task/src/master/url-match/?at=master)