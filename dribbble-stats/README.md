# dribbble-stats

## Task

You have to create a tool to calculate Dribbble stats:

1. For a given Dribbble user find all followers
2. For each follower find all shots
3. For each shot find all "likers"
4. Calculate Top10 "likers"

## Usage

```
> lein run
```
