(ns dribbble-stats.utils)


(defn load-variable
  [key default-token]
  (or (get (System/getenv) key)
      default-token))


(defn pages-number
  [object-count per-page]
  (int (Math/ceil (/ object-count per-page))))


(defn find-first
  [f coll]
  (first (filter f coll)))


(defn indexed
  [coll]
  (map-indexed vector coll))


(defn run-n-funcs
  [func n]
  (doall (map func (map inc (range n)))))
