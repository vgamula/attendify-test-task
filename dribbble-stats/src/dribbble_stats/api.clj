(ns dribbble-stats.api
  (:require [clojure.string :as str]
            [clojure.data.json :as json]
            [org.httpkit.client :as http]))


(def DRIBBLE-API-ENDPOINT "https://api.dribbble.com/v1")


(defn dribbble-get-request
  ([endpoint token page per-page]
   (let [headers {"Authorization" (str "Bearer " token)}
         req-opts {:insecure? true
                   :follow-redirects true
                   :query-params {"per_page" (str per-page)
                                  "page" (str page)}
                   :headers headers}]
     (http/get endpoint req-opts)))
  ([endpoint token page]
   (dribbble-get-request endpoint token page 100))
  ([endpoint token]
   (dribbble-get-request endpoint token 1 100)))


(defn user-info
  [username token]
  (dribbble-get-request
   (str DRIBBLE-API-ENDPOINT "/users/" username)
   token))


(defn user-followers
  [username access-token page]
  (dribbble-get-request
    (str DRIBBLE-API-ENDPOINT "/users/" username "/followers")
    access-token
    page))


(defn user-shots
  [username access-token page]
  (dribbble-get-request
    (str DRIBBLE-API-ENDPOINT "/users/" username "/shots")
    access-token
    page))


(defn shot-likes
  [shot-id access-token page]
  (dribbble-get-request
   (str DRIBBLE-API-ENDPOINT "/shots/" (str shot-id) "/likes")
   access-token
   page))


(defn response-data
  [response]
  ( -> response
       (:body)
       (json/read-str)))


(defn response-future-data
  [fut]
  (response-data (deref fut)))


(defn expand-futures
  [futures-list]
  (vec (apply concat (map response-future-data futures-list))))
