(ns dribbble-stats.core
  (:require [dribbble-stats.api :as dr-api]
            [dribbble-stats.utils :as utils]))


(defn get-shot-likes
  [shot token]
  (let [shot-id (int (get shot "id"))
        likes-page-count (utils/pages-number (int (get shot "likes_count")) 100)
        likes-futures (utils/run-n-funcs #(dr-api/shot-likes shot-id token %) likes-page-count)]
    (dr-api/expand-futures likes-futures)))


(defn get-user-id-from-like
  [like]
  (-> like
      (get "user")
      (get "id")))


(defn print-top-user-likers
  [username token]
  (do
    (println "Wait a minute, we need to retrieve the data.")
    (let [user (dr-api/response-data @(dr-api/user-info username token))
          followers-page-count (utils/pages-number (int (get user "followers_count")) 100)
          shots-page-count (utils/pages-number (int (get user "shots_count")) 100)
          followers-futures (utils/run-n-funcs #(dr-api/user-followers username token %) followers-page-count)
          shots-futures (utils/run-n-funcs #(dr-api/user-shots username token %) shots-page-count)
          followers (vec (map #(get % "follower") (dr-api/expand-futures followers-futures)))
          followers-ids (set (map #(get % "id") followers))
          shots (dr-api/expand-futures shots-futures)
          shots-ids (set (map #(get % "id") shots))
          likes (apply concat (map #(get-shot-likes % token) shots))
          follower-likers-ids (->> likes
                                   (map get-user-id-from-like)
                                   (filter #(contains? followers-ids %)))
          top-likers (->> follower-likers-ids
                          (frequencies)
                          (sort-by val >)
                          (take 10))]
      (do
        (println "Top 10 Likers:")
        (doseq [[idx like-stat] (utils/indexed top-likers)]
          (let [follower-id (key like-stat)
                follower (utils/find-first #(= (get % "id") follower-id) followers)]
            (println (str (inc idx) ". ") (get follower "name") "-" (val like-stat) "Like(s)")))))))



(defn -main [& args]
  (let [username (utils/load-variable "USERNAME" "Sulanov")
        token (utils/load-variable "TOKEN" "759a89d5bcee932e7996d85acf088aa0f07316525a9425fad4ea21e85b4373d9")]
    (print-top-user-likers username token)))
