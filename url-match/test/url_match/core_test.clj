(ns url-match.core-test
  (:require [clojure.test :refer :all]
            [url-match.core :refer :all]))

(def twitter-pattern (new-pattern "host(twitter.com); path(?user/status/?id);"))
(def dribbble-pattern (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"))
(def dribbble2-pattern (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);"))


(deftest twitter-test
  (testing "Default twitter url"
    (let [result (recognize twitter-pattern "http://twitter.com/bradfitz/status/562360748727611392")]
      (is (= result [[:user "bradfitz"] [:id "562360748727611392"]])))))


(deftest dribble-test
  (testing "Proper URL"
    (let [result (recognize dribbble-pattern "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")]
      (is (= result [[:id "1905065-Travel-Icons-pack"] [:offset "1"]]))))
  (testing "Host mistmatch"
    (let [result (recognize dribbble-pattern "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")]
      (is (= result nil))))
  (testing "Offset queryparam mismatch"
    (let [result (recognize dribbble-pattern "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users")]
      (is (= result nil)))))


(deftest dribble2-test
  (testing "Proper URL"
    (let [result (recognize dribbble2-pattern "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")]
      (is (= result [[:id "1905065-Travel-Icons-pack"] [:offset "1"] [:type "users"]])))))
