(ns url-match.core
  (:require [cemerick.url :refer [url]]
            [clojure.string :as str]))


(defn- parse-host
  [host url-parts]
  {:ok (= (:host url-parts) host)})


(defn- parse-path [path url-parts]
  (let [new-regex (-> path
                      (str/replace #"\?([a-z]+)" "([A-Za-z0-9-]+)")
                      (str/replace #"\/" "\\/")
                      (re-pattern))
        variables (->> path
                       (re-seq #"\?([a-z]+)")
                       (map second)
                       (map keyword))
        parsed-result (re-find new-regex (:path url-parts))]
    (if (not parsed-result)
      {:ok false}
      {:ok true
       :result (vec (map vector variables (rest parsed-result)))})))


(defn- parse-queryparam [param url-parts]
  (let [variable (->> param
                      (re-seq #"\?([a-z]+)")
                      (first)
                      (second)
                      (keyword))
        param-key (-> param
                      (str/split #"=")
                      (first))
        query-params (:query url-parts)]
    (if (get query-params param-key)
      {:ok true
       :param-key param-key
       :result [[variable (query-params param-key)]]}
      {:ok false})))


(def parsing-funcs
  {"host" parse-host
   "path" parse-path
   "queryparam" parse-queryparam})


(defn- parse-pattern-part [pattern]
  (let [parts (->> pattern
                   (re-seq #"(?:[0-9A-Za-z.?=/-]+)")
                   (vec))]
    (partial
      (parsing-funcs (parts 0))
      (parts 1))))


(defn new-pattern
  [pattern-string]
  (map parse-pattern-part (str/split pattern-string #" ")))


(defn recognize
  [pattern url-string]
  (let [url-parts (url url-string)
        parsing-result (map #(% url-parts) pattern)]
    (if (not (every? true? (map :ok parsing-result)))
      nil
      (->> parsing-result
           (map :result)
           (filter not-empty)
           (apply concat)
           (vec)))))
